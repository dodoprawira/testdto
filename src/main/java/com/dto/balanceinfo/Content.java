package com.dto.balanceinfo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Content {
	
	private String errorCode;
	private String errorMessage;
	private String bcust;
	private String custDisp;
	private String header3;
	
	private List<GIDIEACCTBALTWSDetailType> gIDIEACCTBALTWSDetailType; 
	
	public String getBcust() {
		return bcust;
	}
	public void setBcust(String bcust) {
		this.bcust = bcust;
	}
	public String getCustDisp() {
		return custDisp;
	}
	public void setCustDisp(String custDisp) {
		this.custDisp = custDisp;
	}
	public String getHeader3() {
		return header3;
	}
	public void setHeader3(String header3) {
		this.header3 = header3;
	}
	public List<GIDIEACCTBALTWSDetailType> getgIDIEACCTBALTWSDetailType() {
		return gIDIEACCTBALTWSDetailType;
	}
	public void setgIDIEACCTBALTWSDetailType(List<GIDIEACCTBALTWSDetailType> gIDIEACCTBALTWSDetailType) {
		this.gIDIEACCTBALTWSDetailType = gIDIEACCTBALTWSDetailType;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
