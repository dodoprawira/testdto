package com.dto.balanceinfo;

import java.util.List;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties
@JsonInclude(Include.NON_NULL)
public class GIDIEACCTBALTWSDetailType {
	private List<MIDIEACCTBALTWSDetailType> midieacctbaltwsDetailType;

	public List<MIDIEACCTBALTWSDetailType> getMidieacctbaltwsDetailType() {
		return midieacctbaltwsDetailType;
	}

	public void setMidieacctbaltwsDetailType(List<MIDIEACCTBALTWSDetailType> midieacctbaltwsDetailType) {
		this.midieacctbaltwsDetailType = midieacctbaltwsDetailType;
	}
	
	
}
